import './public/styles/main.scss'

const menu = document.getElementsByClassName('menu')[0]
const hero = document.getElementsByClassName('hero')[0]
const nav = document.getElementsByTagName('nav')[0]
const overlay = document.getElementsByClassName('sidebar-overlay')[0]
const primaryLinks = document.querySelectorAll('ul.primary-nav > li')

menu.addEventListener('click', (e) => {
  if (nav.classList.contains('show-nav')) {
    menu.classList.add('fa-bars')
    menu.classList.remove('fa-times')
  } else {
    menu.classList.remove('fa-bars')
    menu.classList.add('fa-times')
    overlay.classList.toggle('is-hidden')
  }

  nav.classList.toggle('show-nav')

  e.preventDefault()
})

hero.addEventListener('click', () => {
  if (nav.classList.contains('show-nav')) {
    menu.click()
    menu.classList.add('fa-bars')
    menu.classList.remove('fa-times')
  }
})

for (let link of primaryLinks) {
  link.addEventListener('click', (e) => {
    const link = e.currentTarget
    link.classList.toggle('open')
  })
}
