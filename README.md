# Off-Canvas Template

Off-canvas responsive navigation sample using grid layout and node.


### Installing

Using npm:

```
npm install
npm run start:dev

```

or yarn:

```
yarn install
yarn run start:dev

```

## Demo

A [demo](https://limitless-sierra-86432.herokuapp.com) can be found here.

## Built With

* Express
* Babel/E6
* Webpack
* SCSS-Lint
* Standard JS